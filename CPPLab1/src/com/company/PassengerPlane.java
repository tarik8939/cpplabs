package com.company;

public class PassengerPlane extends Plane  {
    public PassengerPlane(String name, int numberOfPassengers, double maxPassengers, int mass, int range,Flight flight) {
        Name = name;
        NumberOfPassengers = numberOfPassengers;
        MaxPassengers = maxPassengers;
        MassOfPlane = mass;
        FlightRange = range;
        Flight = flight;
    }

    @Override
    public int getMassOfPlane() {
        return MassOfPlane;
    }
    @Override
    public String getName() {
        return Name;
    }
    public double GetMax(){
        return MaxPassengers;
    }
    @Override
    public int getFlightRange() {
        return FlightRange;
    }
    public int NumberOfPassengers;
    public double MaxPassengers;
    public int MassOfPlane;
    private int FlightRange;
    private String Name;

}
