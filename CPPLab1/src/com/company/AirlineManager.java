package com.company;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AirlineManager {
    public  AirlineManager(){
        AllPlane = new ArrayList<Plane>();
        mas =new MassSorting();
    }
    public ArrayList<Plane> AllPlane;
    public Comparator mas;
    public int FindNumberOfSeats()
    {
        int seats = 0;
        for (var item:AllPlane)
        {
            try {
                if (item.getClass().getDeclaredField("MaxPassengers").toString().equals("public double com.company.PassengerPlane.MaxPassengers"))
                    seats+=item.GetMax();
            } catch (NoSuchFieldException e) {
                //e.printStackTrace();
            }
        }
        return seats;
    }
    public double FindLoadCapacity()
    {
        double capacity = 0;
        for (var item:AllPlane)
        {
            try {
                if (item.getClass().getDeclaredField("MaxCapacity").toString().equals("public double com.company.CargoPlane.MaxCapacity"))
                    capacity+=item.GetMax();
            } catch (NoSuchFieldException e) {
                //e.printStackTrace();
            }
        }
        return capacity;
    }

    public void SortByDist(){
        Comparator dist=new DistanceSorting();
        Collections.sort(this.AllPlane,dist);
    }
    public void SortByMass(){
        Collections.sort(this.AllPlane,this.mas);
    }
    public void SortByMassRevers(){
        Collections.sort(this.AllPlane,this.mas.reversed());
    }
    public void SortByRange(){
        this.AllPlane.sort((Plane p1, Plane p2)->p1.getFlightRange()-p2.getFlightRange());
    }
    public void SortByRangeRevers(){
        this.AllPlane.sort((Plane p1, Plane p2)->p2.getFlightRange()-p1.getFlightRange());
    }

    private static class DistanceSorting implements Comparator<Plane>
    {

        @Override
        public int compare(Plane p1, Plane p2) {
            return p1.Flight.route.getDistance()-p2.Flight.route.getDistance();
        }
    }
    private   class MassSorting implements Comparator<Plane>
    {
        @Override
        public int compare(Plane p1, Plane p2) {
            return  p1.getMassOfPlane()-p2.getMassOfPlane();
        }
    }

//    Comparator<Plane> SortByRange = new Comparator<Plane>() {
//        @Override
//        public int compare(Plane p1, Plane p2) {
//            return p1.getFlightRange()-p2.getFlightRange();
//        }
//    };
}
