package com.company;

public class CargoPlane extends Plane {

    public CargoPlane(String name, double capacity, double maxCapacity, int mass, int range,Flight flight) {

        Name = name;
        Capacity = capacity;
        MaxCapacity = maxCapacity;
        MassOfPlane = mass;
        FlightRange = range;
        Flight = flight;

    }
    @Override
    public int getMassOfPlane() {
        return MassOfPlane;
    }
    public double GetMax(){
        return MaxCapacity;
    }

    @Override
    public String getName() {
        return Name;
    }

    @Override
    public int getFlightRange() {
        return FlightRange;
    }

    public double Capacity;
    public double MaxCapacity;
    public int MassOfPlane;
    private int FlightRange;
    private String Name;

}
