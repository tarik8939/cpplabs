package com.company;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {
	// write your code here

        var manager= new AirlineManager();

        manager.AllPlane.add(new PassengerPlane("plane1",20,45,60,80, new Flight(new Route(445))));
        manager.AllPlane.add(new PassengerPlane("plane2",40,85,40,65, new Flight(new Route(55))));
        manager.AllPlane.add(new CargoPlane("plane3",15,55,40,55, new Flight(new Route(85))));
        manager.AllPlane.add(new CargoPlane("plane4",25,85,62,85, new Flight(new Route(115))));
        System.out.println("All seats");
        System.out.println(String.valueOf(manager.FindNumberOfSeats()));
        System.out.println("Max capacity");
        System.out.println(String.valueOf(manager.FindLoadCapacity()));

        System.out.println("Sorted by distance");
//        Comparator dist=new AirlineManager.DistanceSorting();
//        Collections.sort(manager.AllPlane,dist);
        manager.SortByDist();
        for (var item:manager.AllPlane)
        {
            System.out.print(item.getName()+" "+item.Flight.route.getDistance()+"\t");
        }

        System.out.println("\n"+"Sorted by mass");
//        Collections.sort(manager.AllPlane,manager.mas);
        manager.SortByMass();
        for (var item:manager.AllPlane)
        {
            System.out.print(item.getName()+" "+item.getMassOfPlane()+"\t");
        }
        System.out.println("\n"+"Sorted by mass in reverse order");
        manager.SortByMassRevers();
        for (var item:manager.AllPlane)
        {
            System.out.print(item.getName()+" "+item.getMassOfPlane()+"\t");
        }
//        manager.AllPlane.sort((Plane p1, Plane p2)->p1.getFlightRange()-p2.getFlightRange());
        manager.SortByRange();
        System.out.println("\n"+"Sorted by Range");
        for (var item:manager.AllPlane)
        {
            System.out.print(item.getName()+" "+item.getFlightRange()+"\t");
        }
        manager.SortByRangeRevers();
        System.out.println("\n"+"Sorted by Range in reverse order");
        for (var item:manager.AllPlane)
        {
            System.out.print(item.getName()+" "+item.getFlightRange()+"\t");
        }

    }
}
