package com.company;

public abstract class  Plane {

    public Plane(){
    }
    public  abstract double GetMax();

    public String getName() {
        return Name;
    }

    public int getMassOfPlane() {
        return MassOfPlane;
    }

    public int getFlightRange() {
        return FlightRange;
    }

    public Flight Flight;
    private int MassOfPlane;
    private String Name;
    private double MaxMassOfPlane;
    private int FlightRange;
    private double MaxSpeed;
}
