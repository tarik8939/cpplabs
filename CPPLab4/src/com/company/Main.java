package com.company;

import java.util.Scanner;
import java.util.Timer;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        char symbol;
        System.out.println("Press 1 - to use one thread\nPress 2- to use multi-threading");
        symbol = in.next().charAt(0);
        long startTime, milliseconds;
        StringBuilder output = new StringBuilder();
        Node[][] nodes = GetGraph(6);
        Graph graph = new Graph();
        for (var node : nodes[0]) {
            graph.addNode(node);
        }
        DijkstraAlgorithm[] dijkstras = new DijkstraAlgorithm[graph.getNodes().size()];
        for (int i = 0; i < graph.getNodes().size(); ++i) {
            dijkstras[i] = new DijkstraAlgorithm(graph, nodes[i][i]);
        }
// one thread
        if (symbol == '1') {
            startTime = System.currentTimeMillis();
            for (var di : dijkstras) {
                di.calculateShortestPath();
            }
            milliseconds = System.currentTimeMillis() - startTime;
            output.append("\nOne thread time: " + milliseconds + " ms");
        } else if (symbol == '2') {//multithreading
            for (var di : dijkstras) {
                new Thread(new Monitor(di, new Timer())).start();
            }

            startTime = System.currentTimeMillis();
            for (var di : dijkstras) {
                di.start();
            }
            for (var di : dijkstras) {
                try {
                    di.join();
                } catch (InterruptedException e) {
                    System.out.println(di.getName() + " has been interrupted");
                }
            }
            milliseconds = System.currentTimeMillis() - startTime;
            output.append("Multi-thread time: " + milliseconds + " ms");
        }
        DisplayInfo(nodes);
        System.out.println(output);
    }

    public static void DisplayInfo(Node[][] destinations) {
        StringBuilder info = new StringBuilder();
        for (var dest : destinations) {
            for (var node : dest) {
                for (var pathElem : node.getShortestPath()) {
                    info.append(pathElem.getName() + "->");
                }
                info.append(node.getName() + ": " + node.getDistance() + "\n");
            }
            info.append(String.format("-").repeat(20) + "\n");
        }
        System.out.println(info);
    }

    public static Node[][] GetGraph(int count) {
        Node[][] nodes = new Node[count][];
        for (int i = 0; i < count; i++) {
            Node[] node = {
                    new Node("A"),
                    new Node("B"),
                    new Node("C"),
                    new Node("D"),
                    new Node("E"),
                    new Node("F"),

            };
            node[0].addDestination(node[1], 7);
            node[0].addDestination(node[2], 9);
            node[0].addDestination(node[5], 14);
            node[1].addDestination(node[0], 7);
            node[1].addDestination(node[2], 10);
            node[1].addDestination(node[3], 15);
            node[2].addDestination(node[0], 9);
            node[2].addDestination(node[1], 10);
            node[2].addDestination(node[3], 11);
            node[2].addDestination(node[5], 2);
            node[3].addDestination(node[1], 15);
            node[3].addDestination(node[2], 11);
            node[3].addDestination(node[4], 6);
            node[4].addDestination(node[5], 9);

            node[4].addDestination(node[3], 6);
            node[5].addDestination(node[0], 14);
            node[5].addDestination(node[2], 2);
            node[5].addDestination(node[4], 9);
            nodes[i]=node;
        }
        return nodes;
    }
}