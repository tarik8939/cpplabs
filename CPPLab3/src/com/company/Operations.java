package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DataTruncation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Operations {
    public void Read(ArrayList<String> list){
//        String line = "C://Users//Пользователь//IdeaProjects//CPPLab3//src//com//company//File2.txt";
        String line = "C://Users//Пользователь//IdeaProjects//CPPLabs//CPPLab3//src//com//company//File2.txt";
        try {
            FileReader f1 = new FileReader(line);
            StringBuffer buffer = new StringBuffer();
            while (f1.ready()){
                char c = (char) f1.read();
                if (c==' ' || c=='\n'){
                    list.add(buffer.toString());
                    buffer = new StringBuffer();
                } else {
                    buffer.append(c);
                }
            }
            if (buffer.length() > 0)
                list.add(buffer.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void Check(ArrayList<String> list, HashMap<String, Integer> map, HashMap<String, String> map2){
        int c=0,l=0,n=0,o=0;
        for (var word:list) {
                int L = 0, C = 0, N = 0;
                boolean dec = false;
                word = word.toLowerCase();
                for (char ch : word.toCharArray()){
                    for (char charToCheck = 'a'; charToCheck <= 'z'; charToCheck++){
                        if (ch==charToCheck)
                            ++L;
                        else if (ch == '.')
                            ++L;
                        else if (Character.isDigit(ch))
                            continue;
                    }
                    for (char charToCheck = 'а'; charToCheck <= 'я'; charToCheck++){
                        if (ch==charToCheck)
                            ++C;
                        else if (ch == '.')
                            ++C;
                        else if (Character.isDigit(ch))
                            continue;
                    }
                    if (Character.isDigit(ch)){
                        ++N;
                    }
                    else if (ch == ','){
                        ++N;
                        dec = true;
                    }
                }
                var a = word.charAt(word.length()-1);
                if (a=='.' || a == '?'){
                    ++N;
                }
                if (L==word.length())
                    ++l;
                if (C==word.length())
                    ++c;
                else if (N==word.length())
                    ++n;
                else if (N>0 && N<word.length())
                    ++o;
                else if (L < word.length())
                    ++o;
                if (dec == true){
                    map2.put(word,"десяткове");
                }
                else if (dec == false){
                    if (N==word.length())
                        map2.put(word,"ціле");
                }
        }
        map.put("латиниця",l);
        map.put("кирилиця",c);
        map.put("числа",n);
        map.put("жодна з груп",o);
    }

    public String ReadText(){
        try {
            BufferedReader br = new
                    BufferedReader(new FileReader("C://Users//Пользователь//IdeaProjects//CPPLabs//CPPLab3//src//com//company//Text.txt"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null){
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String text = sb.toString();
            return text;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<String> FindSecond(String text, String later){
        var list = new ArrayList<String>();
        Matcher matcher = Pattern.compile("([^.!?]+[.!?])").matcher(text);
        while (matcher.find()){
            int i = 0;
            var line = matcher.group(0);
            var matcher1 = Pattern.compile("([a-zа-яіїґє']+)").matcher(line);
            while (matcher1.find()){
                var a = matcher1.group();
                ++i;
                if (i!=2){
                    continue;
                }
                else if (i == 2){
                    if (a.startsWith(later))
                        list.add(a);
                }
            }
        }
        return list;
    }
    public List<String> FindAns(String text, int len){
        var list = new ArrayList<String>();
        Matcher matcher = Pattern.compile("([^.!?]+[?])").matcher(text);
        while (matcher.find()){
            var line = matcher.group(0);
            var matcher1 = Pattern.compile("([a-zа-яіїґє']+)").matcher(line);
            while (matcher1.find()){
                var a = matcher1.group();
                if (a.length()==len){
                    list.add(a);
                }
            }
        }
        return list;
    }
}

