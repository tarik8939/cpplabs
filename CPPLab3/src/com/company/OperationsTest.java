package com.company;

import org.testng.AssertJUnit;

//import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
public class OperationsTest {
    private AssertJUnit Assert;

    @org.junit.Test
    public void findSecond() {
        var op = new Operations();
        String text = "dfaffreg dgrtgtrg dfgthgrthtrh fdgdftrh.";
        List<String> expected = new ArrayList<String>();
        expected.add("dgrtgtrg");
        List<String> actual = op.FindSecond(text,"d");
        Assert.assertEquals(expected,actual);
    }


    @org.junit.Test
    public void findAns() {
        var op = new Operations();
        String text = "dfaffreg dgrtgtrg dfgthgrthtrh fdgdftrh. zxcef dnbg ddd fghfgh?\n" +
                "dfsg jtyhf qewrg. sdrgf dtyuo grhtyjuk cb?";
        List<String> expected = new ArrayList<String>();
        expected.add("zxcef");
        expected.add("sdrgf");
        expected.add("dtyuo");
        List<String> actual = op.FindAns(text,5);
        Assert.assertEquals(expected,actual);
    }
}