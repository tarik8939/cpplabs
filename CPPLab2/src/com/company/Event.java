package com.company;

public class Event {

    public int Year;
    public int Mounth;
    public int Day;
    public String Name;

    public Event(int year, int mounth, int day, String name) {
        Year = year;
        Mounth = mounth;
        Day = day;
        Name = name;
    }

    @Override
    public String toString() {
        return "Event{" +
                "Year=" + Year +
                ", Mounth=" + Mounth +
                ", Day=" + Day +
                ", Name='" + Name + '\'' +
                "}\n";
    }
}
