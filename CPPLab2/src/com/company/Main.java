package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        ArrayList<Event> list1 = new ArrayList<Event>();
        ArrayList<Event> list2 = new ArrayList<Event>();
        ArrayList<Event> list3 = new ArrayList<Event>();
        ArrayList<ArrayList> genlist = new ArrayList<ArrayList>();

        Map<Integer, ArrayList<Event>> map = new HashMap<Integer, ArrayList<Event>>();
        var creator = new Creator();
        var operation = new Operation();
        creator.Create(list1);
        creator.Create2(list2);
        creator.Create3(list3);
        map.put(2000, list1);
        map.put(2001, list2);
        map.put(2002, list3);
        System.out.println("Sort" );
        for (Map.Entry<Integer, ArrayList<Event>> entry : map.entrySet()) {
            var a = entry.getValue();
            Collections.sort(a, operation);
            System.out.println(a.toString());
        }
        
        var obj = new Delete();
        System.out.println("Delete" );
        for (Map.Entry<Integer, ArrayList<Event>> entry : map.entrySet()) {
            var a = entry.getValue();
            //System.out.println(a.get(0).Year);
            operation.Del(a);
            System.out.println(a.toString());
        }
        //String Reg="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d";
        ArrayList<String> List = new ArrayList<String>();
        ArrayList<String> List2 = new ArrayList<String>();
        Reg reg = new Reg();
        reg.Write("C://Users//Пользователь//IdeaProjects//CPPLab2//src//com//company//File1.txt",List);
        reg.Write("C://Users//Пользователь//IdeaProjects//CPPLab2//src//com//company//File2.txt",List2);

        var Intersection =  operation.Intersection(List,List2);
        for (var i:Intersection) {
            System.out.println(i);
        }

        System.out.println("----------------");
        var s =  operation.Find(Intersection);

//        for (Map.Entry<String, String> entry : s.entrySet()) {
//            var val = entry.getValue();
//            var key = entry.getKey();
//            System.out.print(val);
//        }
    }
}
